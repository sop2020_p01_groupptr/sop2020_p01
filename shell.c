//------------------------------------------------------------------------------
//! \file shell.c
//! defines the shell functions
//
//------------------------------------------------------------------------------
#include "shell.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "triplets_datastruct.h"

int shell_help(char** args)
{
  if(args[1] == NULL)
  {
    printf("triplet_datastruct - UI shell\n");
    printf("-----------------------------\n");
    printf("The following commands exist:\n");

    for(int i = 0; i< (sizeof(funcs_str) / sizeof(char *)); i++)
    {
      printf("  %-12s: %s\n", funcs_str[i], funcs_info_str[i]);
    }

    printf("use \"help <command>\" for more info on a particular command.\n");
    return 1;
  }

  for(int i = 0; i< (sizeof(funcs_str) / sizeof(char *)); i++)
  {
    if(strcmp(args[1], funcs_str[i]) == 0)
    {
      return (*funcs[i])(args);
    }
  }

  printf("unknown syntax, exiting.\n");
  return 0;
} /* shell_help */

static void get_match_input(char* in)
{
  printf(">>");
  //get the next line
  fgets(in, SIZE, stdin);

  //strcspn returns the size of the string not including the chars in the second argument (\n).
  //This is equal to the index of \n if there is a newline and the length of the string if there isn't.
  int newLineIndex = strcspn(in, "\n");

  //if the index is the length of the string, there was no new line. Flush the buffer
  if(newLineIndex == (SIZE - 1))
  {
    emptyBuf(stdin);
  }
  //otherwise remove the \n.
  else
  {
    in[strcspn(in, "\n")] = 0;
  }
} /* getMatchInput */

int shell_match(char** args)
{

  if(strcmp(args[0], "help")==0) //HACK: we call the proper shell function instead of help if there was an function argument after help. Therefore the command (arg 0) is actually help.
  {
    printf("look for a triplet\n");
    printf("the following syntax is understood:\n");
    printf("  match\n");
    printf("    the user will be asked for the strings to match\n");
    printf("  match <s> <p> <o>\n");
    printf("    where s, p and o are the strings to match\n");
    printf("    warning: auto tokenization doesn't allow for quoted strings.\n    Use the first option instead\n");
    printf("    warning: doesn't allow for empty strings.\n    Use the first option instead\n");
    printf("  match <s> <p> <o> <num>\n");
    printf("    where num is the num'th match to return\n");
    printf("    warning: the same rules as above apply\n");
    return 1;
  }

  char s[1024], p[1024], o[1024];
  char num[1024];

  if(args[1]==NULL)
  {
    printf("Enter filter for <s>\n");
    get_match_input(s);
    printf("Enter filter for <p>\n");
    get_match_input(p);
    printf("Enter filter for <o>\n");
    get_match_input(o);
    printf("Enter result number wanted\n");
    get_match_input(num);

    if(!match(s, p, o, atoi(num)))
    {
      printf("(%s, %s, %s)\n", s, p, o);
    }
    else
    {
      printf("no match found\n");
    }

    return 1;
  }

  if ((args[1]!=NULL) && (args[2]!=NULL) && (args[3]!=NULL))
  {
    strcpy(s, args[1]);
    strcpy(p, args[2]);
    strcpy(o, args[3]);
    int i = 0;

    if(args[4]!=NULL)
    {
      i = atoi(args[4]);
    }

    if(!match(s, p, o, i))
    {
      printf("(%s, %s, %s)\n", s, p, o);
    }
    else
    {
      printf("no match found\n");
    }

    return 1;
  }

  return 0;
} /* shell_match */

int shell_insert(char** args)
{
  if(strcmp(args[0], "help")==0) //HACK: we call the proper shell function instead of help if there was an function argument after help. Therefore the command (arg 0) is actually help.
  {
    printf("add a new triplet into the list\n");
    printf("the following syntax is understood:\n");
    printf("  insert\n");
    printf("    the user will be asked for the strings to add\n");
    printf("  insert <s> <p> <o>\n");
    printf("    where s, p and o and the strings to add to the triplet\n");
    printf("    warning: auto tokenization doesn't allow for quoted strings.\n    Use the first option instead\n");
    return 1;
  }

  if(args[1] == NULL)
  {
    int ret = get_triplet(stdin);

    switch(ret)
    {
      case 0:
       printf("%s\n", "New triplet added succesfully!");
       break;

      case 1:
       printf("%s\n", "One of the strings was too long. No new triplet added!");
       break;

      case 2:
       printf("%s\n", "Triplet already exists. No new triplet added.");
       break;

      case 3:
       printf("%s\n", "Error during memory allocation. No new triplet added.");
       break;

      case 4:
       printf("%s\n", "Triplets are not allowed to contain empty strings, no new triplet added.");
       break;

      default:
       break;
    } /* switch */

    return 1;
  }

  if((args[1] != NULL) && (args[2] != NULL) && (args[3] != NULL))
  {
    insert(args[1], args[2], args[3]);
    printf("new triplet added\n");
    return 1;
  }

  printf("unknown syntax, exiting.\n");
  return 0;
} /* shell_insert */

int shell_print(char** args)
{
  if(strcmp(args[0], "help")==0) //HACK: we call the proper shell function instead of help if there was an function argument after help. Therefore the command (arg 0) is actually help.
  {
    printf("print the list of triplets\n");
    printf("the following syntax is understood:\n");
    printf("  print\n");
    printf("    print the list of triplets\n");
    printf("  print reverse\n");
    printf("    print the list of triplets in reverse order\n");
    return 1;
  }

  if(args[1] == NULL)
  {
    print_list(0);
    return 1;
  }

  if(strcmp(args[1], "reverse") == 0)
  {
    print_list(1);
    return 1;
  }

  printf("unknown syntax, exiting.\n");
  return 0;
} /* shell_print */

int shell_grep(char** args)
{
  if(strcmp(args[0], "help")==0) //HACK: we call the proper shell function instead of help if there was an function argument after help. Therefore the command (arg 0) is actually help.
  {
    printf("delete a triplet\n");
    printf("the following syntax is understood:\n");
    printf("  grep <regex>\n");
    printf("    grep the list and print any matching triplet\n");
    printf("  grep <regex> <head> <num>\n");
    printf("    grep the list and print the num first matches\n");
    printf("  grep <regex> <tail> <num>\n");
    printf("    grep the list and print the num last matches\n");
    return 1;
  }

  if(args[2] == NULL)
  {
    grep(args[1], 1, getSize());
    return 1;
  }

  if((args[2] != NULL) && (args[3] != NULL))
  {
    if(strcmp(args[2], "head")==0)
    {
      grep(args[1], 1, atoi(args[3]));
      return 1;
    }

    if(strcmp(args[2], "tail")==0)
    {
      grep(args[1], 0, atoi(args[3]));
      return 1;
    }
  }

  printf("unknown syntax, exiting.\n");
  return 0;
} /* shell_grep */

int shell_delete(char** args)
{
  if(strcmp(args[0], "help")==0) //HACK: we call the proper shell function instead of help if there was an function argument after help. Therefore the command (arg 0) is actually help.
  {
    printf("delete a triplet\n");
    printf("the following syntax is understood:\n");
    printf("  delete\n");
    printf("    delete the last triplet\n");
    return 1;
  }

  if(args[1] == NULL)
  {
    if(delete_node(tail)==1)
    {
      printf("%s\n", "No triplet to delete");
    }

    return 1;
  }

  printf("unknown syntax, exiting.\n");
  return 0;
} /* shell_delete */

int shell_exit(char** args)
{
  if(strcmp(args[0], "help")==0) //HACK: we call the proper shell function instead of help if there was an function argument after help. Therefore the command (arg 0) is actually help.
  {
    printf("exit the shell\n");
    return 1;
  }

  return 0;
}

int shell_occurences(char** args)
{
  if(strcmp(args[0], "help")==0) //HACK: we call the proper shell function instead of help if there was an function argument after help. Therefore the command (arg 0) is actually help.
  {
    printf("Count the number of occurences of a char or a string\n");
    printf("the following syntax is understood:\n");
    printf("  occurences <char>\n");
    printf("    count the occurrances of a char\n");
    printf("  occurences <string>\n");
    printf("    count the occurrances of a string\n");
    printf("    warning: auto tokenization doesn't allow for quoted strings.\n");
    return 1;
  }

  if(strlen(args[1]) == 1)
  {
    printf("%i\n", getOccurencesOfChar(*args[1]));
    return 1;
  }
  else if(strlen(args[1])>1)
  {
    printf("%i\n", getOccurencesOfString(args[1]));
    return 1;
  }

  printf("unknown syntax, exiting.\n");
  return 0;
} /* shell_occurences */

int shell_file(char**args)
{
  if(strcmp(args[0], "help")==0) //HACK: we call the proper shell function instead of help if there was an function argument after help. Therefore the command (arg 0) is actually help.
  {
    printf("Get triplets from a file\n");
    printf("the following syntax is understood:\n");
    printf("  file <path>\n");
    printf("    get triplets from the file defined by <path>\n");
    printf("    warning: auto tokenization doesn't allow for quoted strings.\n");
    return 1;
  }

  if(args[1] == NULL)
  {
    printf("unknown syntax, exiting.\n");
    return 0;
  }

  FILE *f = fopen(args[1], "r");

  if(f == NULL)
  {
    printf("Error opening file, exiting.\n");
    return 0;
  }

  printf("%i triplets added\n", read_file(f));
  return 1;

} /* shell_file */

void   shell_loop()
{
  char* line;
  char**args;
  int   status;

  shell_help(shell_split_line("help"));

  do
  {
    printf("> ");
    line   = shell_read_line();
    args   = shell_split_line(line);
    status = shell_execute(args);

    free(line);
    free(args);
  }
  while(status);
} /* shell_loop */

char*  shell_read_line()
{
  char*  line    = NULL;
  size_t bufsize = 0;//getline will allocate buffer.

  if(getline(&line, &bufsize, stdin) == -1)
  {
    if(feof(stdin))
    {
      exit(0); //EOF received
    }
    else
    {
      perror("readline");
      exit(1);
    }
  }

  return line;
} /* shell_read_line */

char** shell_split_line(char* line)
{
  int   bufsize = 1024, position = 0;
  char**tokens = calloc(bufsize, sizeof(char*));
  char* token;

  if(!tokens)
  {
    fprintf(stderr, "shell: allocation error\n");
    exit(1);
  }

  token = strtok(line, " \t\r\n\a"); //split line into tokens and geta pointer to the first token

  while(token != NULL)
  {
    tokens[position] = token;
    position++;

    token = strtok(NULL, " \t\r\n\a"); //remove the chars inbetween tokens
  }

  tokens[position] = NULL;
  return tokens;
} /* shell_split_line */

int    shell_execute(char**args)
{
  if(args[0] == NULL)
  {
    //empty command
    return 1;
  }

  for(int i = 0; i< (sizeof(funcs_str) / sizeof(char *)); i++) //go through the array of shell function names
  {
    if(strcmp(args[0], funcs_str[i]) == 0) //and match the correct function
    {
      return (*funcs[i])(args); //call function through function pointer
    }
  }

  printf("Unknown command: %s\n", args[0]);
  printf("Use \"help\" for a list of available commands\n");

  return 1;
} /* shell_execute */