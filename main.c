#include "triplets_datastruct.h"
#include "test.h"
#include "shell.h"
#ifdef wasm
#include <emscripten.h>
#endif

#ifdef wasm
EMSCRIPTEN_KEEPALIVE
#endif
int main()
{
  #ifndef wasm
  //functional_test();
  //memory_leak_test();
  //file_input_test();
  //regex_test();
  shell_loop();
  #endif
  return 0;
} /* main */