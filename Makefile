#convenience define, if no target is specified, local target is assumed.
ifeq ($(TARGET),)
	TARGET=main
endif

#include the target specific file
include $(TARGET).make

#compiler options
CFLAGS += -c -Wall -Os

#linker
#	$() is used to use the value of the variable (Similar to bash variable handling).
# The linker is used to link all the .o files into one program. Here we use the same tool to compile and to link (gcc).
LD = $(CC)
#linker options
LDFLAGS +=

#List of all objects that should get created:
#	$(wilcard *.c) will match all files ending in ".c" in the current directory
#	patsubst %.c, %.o will substitute .c with .o on every matched file string (not the file itself) and store the results in OBJECTS
OBJECTS = $(patsubst %.c, %.obj, $(wildcard *.c))

#If make is called without target name, the first target encountered in a Makefile will be created.
#make sure it's the target we want.
all: $(TARGET)

#Our target (defined in the target specific .make file) should depend on all the .o targets / files.
#Therefore the whole list of objects (see line 22) is added as a prerequisite to this target.
#
# $(LD) gets replaced by the value of LD --> $(CC) --> gcc or emcc.
# $@ recalls the text left of the ":". Here that would be $(TARGET), i.e. main
# $^ recalls every text item on the right side of the ":". Here that would be the list of objects created on line 22.
# $(LDFLAGS) holds all the linker specific flags/options. Currently empty
$(TARGET): $(OBJECTS)
	$(LD) -o $@$(EXT) $^ $(LDFLAGS)

#This target creates all the object files:
# %.o will match any target call ending in .obj (Here it will be called for every item on OBJECTS because it is a prerequisite for TARGET (line 35))
# %.c will take the "%" part of the target and replace it here to make that item a prerequisite (e.g: for test.o the prerequisite would be test.c)
#		This together with the objects array created at line 22 gives us the following:
#			Take all .c files, save their names in an array but change the extension to .obj (line 22)
#			To link the program we need all the object files. (line 35)
#			for all targets ending in .obj do the following: (line 48)
#				<compiler> <flags> <item after the :> -o <item before the :>
# this results in every .c file being compiled and saved under the same name but with a .obj extension.
%.obj: %.c
	$(CC) $(CFLAGS) $^ -o $@

#make expects at least one output file for ever target.
#That's how make itself checks if the target has worked: "is there a new file in the system or has an old file been updated? (Similar to how it checks if anything needs to be done at all)
#
#The clean and run targets doesn't create a file. Therefore make would throw a warning.
#Specifying clean and run as .PHONY targets will tell make not to expect a new file.
.PHONY: clean run

#remove the program and all object files.
# >/dev/null redirects the stdout to null (no output)
#	2>&1 redirects stderr to where stdout no goes (null -> no output)
# || is the logical or operator. The right side of it will be evaluated only if the left side returns a non-zero exit code.
# : is an empty command, it returns true immediately. This will make sure that the line 63 always returns true,
#   which will tell make that all went well even if there was no file to delete.
clean:
	#hacky way of making sure that main.c and the target specific .make files don't get deleted: rename them
	mv main.c main_.c
	mv $(TARGET).make $(TARGET)_.make
	rm $(TARGET) $(TARGET).* $(OBJECTS) >/dev/null 2>&1 || :
	mv main_.c main.c
	mv $(TARGET)_.make $(TARGET).make

#start the webbrowser with emrun
run:
	emrun --kill_start --port 8080 .


#rebuild target: call make clean followed by make all.
rebuild: clean all
