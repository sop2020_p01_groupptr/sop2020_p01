//------------------------------------------------------------------------------
//! \file triplets_datastruct.c
//! defines the triplets data structure and its associated functions.
//
//------------------------------------------------------------------------------

#include "triplets_datastruct.h"
#ifdef wasm
#include <emscripten.h>
#endif
#include "test.h"
#include <string.h>
#include <regex.h>

//Define two pointers to the head and tail elements of the list and initialize them to NULL
linked_list_node *head = NULL;
linked_list_node *tail = NULL;
int item_count = 0;

#ifdef wasm
EMSCRIPTEN_KEEPALIVE
#endif
int insert(char* s, char* p, char* o)
{
  if((strlen(s)>SIZE) || (strlen(p)>SIZE) || (strlen(o)>SIZE)) {return 1;}

  if((*s == '\0') || (*p == '\0') || (*o == '\0'))
  {
    return 2;
  }

  //allocate memory for a new triplet
  char *_s = (char*)calloc(SIZE, sizeof *_s);
  char *_p = (char*)calloc(SIZE, sizeof *_p);
  char *_o = (char*)calloc(SIZE, sizeof *_o);

  if((_s == NULL) || (_p == NULL) || (_o == NULL))
  {
    //failed to allocate memory
    fprintf(stderr, "Couldn't allocate memory\n");
    return 3;
  }

  //copy string over
  strcpy(_s, s);
  strcpy(_p, p);
  strcpy(_o, o);
  //instantiate the new triplet
  triplet t = {_s, _p, _o};

  //allocate memory for a new node
  linked_list_node *tmp = (linked_list_node*)calloc(1, sizeof *tmp);

  if (tmp == NULL)
  {
    //failed to allocate memory
    fprintf(stderr, "Couldn't allocate memory\n");
    return 3;
  }

  //fill the node with data and NULL pointers.
  tmp->data = t;
  tmp->next = NULL;
  tmp->prev = NULL;

  //check if list is empty
  if((head == NULL) || (tail == NULL))
  {
    //head and tail point to the only node in the list
    head = tmp;
    tail = tmp;
  }
  else
  {
    tail->next = tmp;  //!< last element's next points to the new node
    tmp->prev  = tail; //!< new node's previous is the current last node of the list
  }

  tail = tmp; //!< tail should point to the new node.

  item_count++;
  return 0;
} /* insert */

#ifdef wasm
EMSCRIPTEN_KEEPALIVE
#endif
int isZero(char* string)
{
  char* look = string;
  char  c;

  while ((c = *look++))
  {
    if(c!='0')
    {
      return 0;
    }
  }

  return 1;
} /* isZero */

#ifdef wasm
EMSCRIPTEN_KEEPALIVE
#endif
int compare(char*s, char*p, char*o, triplet data)
{
  if(!isZero(s))
  {
    if(strcmp(s, data.s) !=0) {return 0;}
  }

  if(!isZero(p))
  {
    if(strcmp(p, data.p) !=0) {return 0;}
  }

  if(!isZero(o))
  {
    if(strcmp(o, data.o) !=0) {return 0;}
  }

  return 1;
} /* cmp */

linked_list_node* find(char *s, char*p, char*o, long result)
{
  //check if list is empty
  if((head == NULL) || (tail == NULL) )
  {
    return NULL;
  }

  //if all strings are defined there is only one match. result > 0 is not possible
  if(!isZero(s) && !isZero(p) && !isZero(o) && (result>0))
  {
    return NULL;
  }

  int found = 0;
  int index = -1;
  linked_list_node *tmp = head;

  //traverse the list as long as next elements exist and the number of matches hasn't exceeded result
  while(tmp != NULL && index<result)
  {
    //if it's a match, increase counter
    if(compare(s, p, o, tmp->data)) {index++;}

    //if the counter has reached result, we have found a match
    if(index==result) {found = 1;}

    //if no match found, go to the next element
    if(!found) {tmp = tmp->next;}
  }

  return found ? tmp : NULL;

} /* find */

#ifdef wasm
EMSCRIPTEN_KEEPALIVE
#endif
int match(char* s, char* p, char* o, long result)
{
  linked_list_node * tmp = find(s, p, o, result);

  //Fill missing fields if a match was found
  if (tmp!=NULL)
  {
    if(isZero(s))
    {
      strcpy(s, tmp->data.s);
    }

    if(isZero(p))
    {
      strcpy(p, tmp->data.p);
    }

    if(isZero(o))
    {
      strcpy(o, tmp->data.o);
    }

    return 0;
  }

  return 1;
} /* match */

#ifdef wasm
EMSCRIPTEN_KEEPALIVE
#endif
int delete_node(linked_list_node *tmp)
{
  //delete the node if it exists
  if (tmp!=NULL)
  {
    //the node to delete is the only node.
    if((tmp == tail) && (tmp == head))
    {
      head = NULL;
      tail = NULL;
    }
    //the node to delete is the head
    else if(tmp == head)
    {
      head = tmp->next;
      tmp->next->prev = NULL;
    }
    //the node to delete is the tail
    else if (tmp == tail)
    {
      tail = tmp->prev;
      tmp->prev->next = NULL;
    }
    //the node is somewhere in the list
    else
    {
      tmp->prev->next = tmp->next;
      tmp->next->prev = tmp->prev;
    }

    //free allocated memory of the triplet
    free(tmp->data.s);
    free(tmp->data.p);
    free(tmp->data.o);
    //remove dangling pointers
    tmp->data.s = NULL;
    tmp->data.p = NULL;
    tmp->data.o = NULL;

    //free allocated memory of the linked list node
    free(tmp);
    //remove dangling pointer
    tmp = NULL;
    item_count--;
    return 0;
  }

  //trying to delete a non-existant node
  return 1;
} /* delete_node */

#ifdef wasm
EMSCRIPTEN_KEEPALIVE
#endif
int delete_match(char* s, char* p, char* o, long result)
{
  linked_list_node * tmp = find(s, p, o, result);

  return delete_node(tmp);

} /* delete */

int getSize()
{
  return item_count;
}

int getOccurencesOfChar(char c)
{
  int count = 0;
  linked_list_node *tmp = head;

  do
  {
    char* occ;
    occ = strchr(tmp->data.s, c);

    while(occ!=NULL)
    {
      count++;
      occ = strchr(occ + 1, c);
    }

    occ = strchr(tmp->data.p, c);

    while(occ!=NULL)
    {
      count++;
      occ = strchr(occ + 1, c);
    }

    occ = strchr(tmp->data.o, c);

    while(occ!=NULL)
    {
      count++;
      occ = strchr(occ + 1, c);
    }
  }
  while((tmp = tmp->next) != NULL);

  return count;
} /* getOccurencesOfChar */

#ifdef wasm
EMSCRIPTEN_KEEPALIVE
#endif
int getOccurencesOfString(char* s)
{
  int count = 0;
  linked_list_node *tmp = head;

  do
  {
    char* occ;
    occ = strstr(tmp->data.s, s);

    while(occ!=NULL)
    {
      count++;
      occ = strstr(occ + 1, s);
    }

    occ = strstr(tmp->data.p, s);

    while(occ!=NULL)
    {
      count++;
      occ = strstr(occ + 1, s);
    }

    occ = strstr(tmp->data.o, s);

    while(occ!=NULL)
    {
      count++;
      occ = strstr(occ + 1, s);
    }
  }
  while((tmp = tmp->next)!=NULL);

  return count;
} /* getOccurencesOfString */

#ifdef wasm
EMSCRIPTEN_KEEPALIVE
#endif
int read_file(FILE* f)
{
  //count how many items we've added
  int  count = 0;

  char s[SIZE] = {'\0'}, p[SIZE] = {'\0'}, o[SIZE] = {'\0'};

  while(!feof(f))
  {
    //we could catch the input error here if needed, but insert will fail with an partly empty triplet anyways.
    get_input(f, s, 0);
    get_input(f, p, 0);
    get_input(f, o, 0);

    if(insert(s, p, o)!=0)
    {
      break; //break as soon as we have an error.
    }

    count++;
    //reset strings, otherwise get_input will beak.
    *s = '\0';
    *p = '\0';
    *o = '\0';
  }

  return count;

} /* read_file */

#ifdef wasm
EMSCRIPTEN_KEEPALIVE
#endif
int get_triplet(FILE* f)
{

  char s[SIZE] = {'\0'}, p[SIZE] = {'\0'}, o[SIZE] = {'\0'};

  printf("%s\n>> ", "Please enter the first member of the triplet");

  get_input(stdin, s, 1);

  printf("%s: %s\n", "Entered", s);

  printf("%s\n>> ", "Please enter the second member of the triplet");

  get_input(stdin, p, 1);

  printf("%s: %s\n", "Entered", p);

  printf("%s\n>> ", "Please enter the third member of the triplet");

  get_input(stdin, o, 1);

  printf("%s: %s\n", "Entered", o);

  return insert(s, p, o);

} /* get_input */

#ifdef wasm
EMSCRIPTEN_KEEPALIVE
#endif
int get_input(FILE* f, char* input, int interactive)
{
  //max number of attempts to read a line is 10.
  int attempts      = 10;
  int attempt_count = 0;

  while(attempt_count<attempts && !feof(f))
  {
    //get the next line
    fgets(input, SIZE, f);

    //strcspn returns the size of the string not including the chars in the second argument (\n).
    //This is equal to the index of \n if there is a newline and the length of the string if there isn't.
    int newLineIndex = strcspn(input, "\n");

    //if the index is the length of the string, there was no new line. Flush the buffer
    if(newLineIndex == (SIZE - 1))
    {
      emptyBuf(f);
    }
    //otherwise remove the \n.
    else
    {
      input[strcspn(input, "\n")] = 0;
    }

    //return if the string is not empty
    if(*input!='\0')
    {
      return 0;
    }

    ++attempt_count;

    if(interactive)
    {
      printf("%s\n>> ", "Empty strings are not allowed, please try again.");
    }
  }

  return 1;
} /* get_input */

#ifdef wasm
EMSCRIPTEN_KEEPALIVE
#endif
void emptyBuf(FILE* f)
{
  int c = 0;

  while (c != '\n' && c != EOF)
  {
    c = getc(f);
  }
}

#ifdef wasm
EMSCRIPTEN_KEEPALIVE
#endif
int print_index(int index)
{

  linked_list_node* tmp = head;

  //loop until the right index is reached
  for(int i = 0; i<index; ++i)
  {
    //arrived at end of list (or list was empty)
    if(tmp == NULL)
    {
      fprintf(stderr, "Element with index %i doesn't exist", index);
      return 1;
    }

    tmp = tmp->next;
  }

  //if index was length of list, tmp is now null.
  if(tmp == NULL)
  {
    fprintf(stderr, "Element with index %i doesn't exist", index);
    return 1;
  }

  print_triplet(tmp->data);
  return 0;
} /* print_index */

#ifdef wasm
EMSCRIPTEN_KEEPALIVE
#endif
void print_triplet(triplet t)
{
  printf("(%s, %s, %s)", t.s, t.p, t.o);
}

#ifdef wasm
EMSCRIPTEN_KEEPALIVE
#endif
char* string_triplet(triplet t)
{
  char *ret = (char*)calloc(3 * 1024, sizeof(ret));

  sprintf(ret, "(%s, %s, %s)", t.s, t.p, t.o);
  return ret;
}

#ifdef wasm
EMSCRIPTEN_KEEPALIVE
#endif
char* string_list()
{
  char* ret = (char*)calloc(getSize() * 1024 * 3, sizeof(ret));
  linked_list_node*tmp;

  tmp = head;
  strcat(ret, "START -> ");

  while(tmp != NULL)
  {
    strcat(ret, string_triplet(tmp->data));
    strcat(ret, " -> ");
    tmp = tmp->next;
  }

  strcat(ret, "END\n");
  return ret;

} /* get_print */

#ifdef wasm
EMSCRIPTEN_KEEPALIVE
#endif
void print_list(int reverse)
{
  linked_list_node*tmp;

  if(reverse)
  {
    tmp = tail;
    printf("END -> ");

    while(tmp != NULL)
    {
      print_triplet(tmp->data);
      printf(" -> ");
      tmp = tmp->prev;
    }

    printf("START\n");
  }
  else
  {
    tmp = head;
    printf("START -> ");

    while(tmp != NULL)
    {
      print_triplet(tmp->data);
      printf(" -> ");
      tmp = tmp->next;
    }

    printf("END\n");
  }
} /* print_list */

#ifdef wasm
EMSCRIPTEN_KEEPALIVE
#endif
int regexp(regex_t* regex, triplet*  data)
{
  if(!regexec(regex, data->s, 0, NULL, 0))
  {
    print_triplet(*data);
    printf("\n");
    return 1;
  }

  if(!regexec(regex, data->p, 0, NULL, 0))
  {
    print_triplet(*data);
    printf("\n");
    return 1;
  }
  else if(!regexec(regex, data->o, 0, NULL, 0))
  {
    print_triplet(*data);
    printf("\n");
    return 1;
  }

  return 0;
} /* regexp */

#ifdef wasm
EMSCRIPTEN_KEEPALIVE
#endif
int grep(char* regex, int bool_head, int amount)
{
  //generate the regex
  regex_t re;

  if(regcomp(&re, regex, REG_EXTENDED))
  {
    fprintf(stderr, "Could not compile regex\n");
    return 0;
  }

  //set the starting point
  linked_list_node * tmp;

  tmp = bool_head ? head : tail;

  int counter = 0;

  //amount == -1 means match all: &&1 is always true
  while(tmp!=NULL && (amount<0 ? 1 : counter<amount))
  {
    counter += regexp(&re, &(tmp->data));
    //jump to next node
    tmp      = bool_head ? tmp->next : tmp->prev;
  }

  regfree(&re);
  return counter;
} /* grep */