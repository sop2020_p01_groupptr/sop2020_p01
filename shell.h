//------------------------------------------------------------------------------
//! \file shell.h
//! declares the shell functions
//
//------------------------------------------------------------------------------

#ifndef _SHELL_H_
#define _SHELL_H_

/*!
 *  \brief shell help command
 *
 *  \param [in] args pointer ot an array of arguments
 *  \return 1 on success, 0 otherwise
 **/
int shell_help(char** args);
/*!
 *  \brief shell insert command
 *
 *  \param [in] args pointer ot an array of arguments
 *  \return 1 on success, 0 otherwise
 **/
int shell_insert(char** args);
/*!
 *  \brief shell match command
 *
 *  \param [in] args pointer ot an array of arguments
 *  \return 1 on success, 0 otherwise
 **/
int shell_match(char** args);
/*!
 *  \brief shell print command
 *
 *  \param [in] args pointer ot an array of arguments
 *  \return 1 on success, 0 otherwise
 **/
int shell_print(char** args);
/*!
 *  \brief shell grep command
 *
 *  \param [in] args pointer ot an array of arguments
 *  \return 1 on success, 0 otherwise
 **/
int shell_grep(char** args);
/*!
 *  \brief shell delete command
 *
 *  \param [in] args pointer ot an array of arguments
 *  \return 1 on success, 0 otherwise
 **/
int shell_delete(char** args);
/*!
 *  \brief shell exit command
 *
 *  \param [in] args pointer ot an array of arguments
 *  \return 1 on success, 0 otherwise
 **/
int shell_exit(char** args);
/*!
 *  \brief shell occurences command
 *
 *  \param [in] args pointer ot an array of arguments
 *  \return 1 on success, 0 otherwise
 **/
int shell_occurences(char** args);

/*!
 *  \brief shell file command
 *
 *  \param [in] args pointer to an array of arguments
 *  \return 1 on success, 0 otherwise
 **/
int shell_file(char** args);

/*!
 *  \brief name of shell functions
 */
static char* funcs_str[] = {"help", "insert", "print", "grep", "match", "delete", "exit", "occurences", "file"};

/*!
 *  \brief info strings for shell functions
 */
static char* funcs_info_str[] = {"print this screen", "add a new triplet ot the list", "print the list of triplets", "grep the list for matching triplets", "search for a match", "delete a triplet", "exit the shell", "count occurences of a char or a string", "get triplets from a file"};

/*!
 *  \brief function pointers for shell functions
 */
static int (*funcs[]) (char **) = {&shell_help, &shell_insert, &shell_print, &shell_grep, &shell_match, &shell_delete, &shell_exit, &shell_occurences, &shell_file};

/*!
 *  \brief main shell loop
 *
 **/
void shell_loop();

/*!
 *  \brief read line function for shell
 *
 *  \return pointer to the line read
 **/
char* shell_read_line();

/*!
 *  \brief tokenizer for shell
 *
 *  \param [in] line line to be tokenized
 *  \return pointer to array of tokens
 **/
char** shell_split_line(char* line);

/*!
 *  \brief shell execution function
 *
 *  \param [in] args pointer to array of command arguments
 *  \return 1 on success, 0 otherwise
 **/
int shell_execute(char**args);

#endif /* end of include guard: _SHELL_H_ */