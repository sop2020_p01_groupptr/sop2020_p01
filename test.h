//------------------------------------------------------------------------------
//! \file test.h
//! declares test functions
//
//------------------------------------------------------------------------------
#ifndef _TEST_H_
#define _TEST_H_

#include <stdlib.h>
#include <stdio.h>
#include "triplets_datastruct.h"

/**
 *  Test functionality of all functions
 *
 **/
void functional_test();

/**
 *  Memory leak test
 *
 **/
void memory_leak_test();

/**
 *  file inout test
 *
 **/
void file_input_test();

/**
 *  regex test
 *
 **/
void regex_test();

#endif /* end of include guard: _TEST_H_ */