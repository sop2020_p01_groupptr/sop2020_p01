//------------------------------------------------------------------------------
//! \file triplets_datastruct.h
//! declares the triplets data structure and its associated functions.
//
//------------------------------------------------------------------------------

#ifndef _TRIPLETS_DATASTRUCT_H_
#define _TRIPLETS_DATASTRUCT_H_

#include <stdlib.h>
#include <stdio.h>
#include <regex.h>

//Maximum size of a string
#define SIZE 1024

//Definition of a data triplets struct
//A triplet contains three strings
typedef struct triplet
{
  char* s;
  char* p;
  char* o;
}triplet;

//Definition of a double linked list node
//A node contains a triplet, a pointer to the previous node and a pointer to the next node
typedef struct linked_list_node
{
  triplet data;
  struct linked_list_node *prev;
  struct linked_list_node *next;
}linked_list_node;

extern linked_list_node *head;
extern linked_list_node *tail;
extern int item_count;

/**
 *  Insert a new node at the end of the list.
 *
 *  Non-zero exit code if the triplet already exists,
 *  if the memory allocation fails or
 *  if the strings are longer than SIZE characters.
 *
 *  \param s head of the first string
 *  \param p head of the second string
 *  \param o head of the third string
 *
 *  \return 0 on success
 **/
int insert(char* s, char* p, char* o);

/**
 *  check if a string is filled with zeroes
 *
 *  \param s head of the string
 *  \return 1 if the string is empty, 0 otherwise
 **/
int isZero(char*s);

/**
 *  compare a set of strings and a triplet.
 *
 *  If a string is empty (\see isZero()), no comparison is performed.
 *  Comparison succeeds on an empty set.
 *
 *  \param s head of the first string
 *  \param p head of the second string
 *  \param o head of the third string
 *  \param t triplet
 *
 *  \return true on match, false otherwise
 **/
int compare(char*s, char*p, char*o, triplet data);

/**
 *  find a node in the list given strings and an index
 *
 *  \param s head of the first string
 *  \param p head of the second string
 *  \param o head of the third string
 *  \param result the index of the node in the filtered list.
 *
 *  \return pointer to a linked_list_node matching the paramters
 **/
linked_list_node* find(char *s, char*p, char*o, long result);

/**
 *  match a triplet with a set of strings and an index
 *
 *  Filters the list according to the strings and copies the triplet's data into the empty strings
 *
 *  \param s head of the first string
 *  \param p head of the second string
 *  \param o head of the third string
 *  \param result the index of the node in the filtered list.
 *
 *  \return 0 on success, 1 otherwise
 **/
int match(char* s, char* p, char* o, long result);

/**
 *  delete a node from the list
 *
 *  \param tmp pointer to the node
 *
 *  \return 0 if a node was deleted, 1 otherwise
 **/
int delete_node(linked_list_node *tmp);

/**
 *  delete a node from the list
 *
 *  \param s head of the first string
 *  \param p head of the second string
 *  \param o head of the third string
 *  \param result the index of the node in the filtered list.
 *
 *  \return 0 if a node was deleted, 1 otherwise
 **/
int delete_match(char* s, char* p, char* o, long result);

/**
 *  get number of items in the list
 *
 **/
int getSize();

/**
 *  Count and return the occurences of a character
 *
 *  \param c the char to count
 **/
int getOccurencesOfChar(char c);

/**
 *  Count and return the occurences of a string
 *
 *  \param s the string to count
 **/
int getOccurencesOfString(char* s);

/**
 *  print a triplet given an index
 *
 *  \param index the index of the triplet in the list
 *
 *  \return 0 on success, 1 otherwise
 **/
int print_index(int index);

/**
 *  print a triplet
 *
 *  \param t the triplet to print
 **/
void print_triplet(triplet t);

/**
 *  print the linked list
 *
 *  \param reverse if the list should be printed starting from the tail or not
 **/
void print_list(int reverse);

/**
 *  read a file and add the resulting triplets to the linked list
 *
 *  The function reads lines one after the other and every 3 successfully read lines will create a triplet.
 *
 *  \param f pointer to the file to read.
 *
 *  \return the number of triplets added.
 **/
int read_file(FILE* f);

/**
 *  get data for a new triplet from the user (stdin) or from a file
 *
 *  \param f pointer to a filestream. stdin is valid ;)
 **/
int get_triplet(FILE* f);

/**
 *  get a string from the user (stdin) or from a file and store it in input
 *
 *  \param f pointer to a filestream. stdin is valid :)
 *  \param input pointer to a char array.
 *  \param interactive set this to 1 to get notified about an invalid input.
 **/
int get_input(FILE* f, char* input, int interactive);

/**
 *  empty the filestream buffer (needed because get_input uses fgets which doesn't truncate strings in case of buffer overflow)
 *
 *  \param f pointer to the filestream to empty. stdin is valid :)
 **/
void emptyBuf(FILE* f);

/*!
 *  grep the triplet with a regular expression and print the triplet if it matches
 *
 *  A triplet matches if at least one string matches the regex.
 *
 *  \param  regex the regex to match against
 *  \param data the triplet to match
 *
 *  \return 1 if the triplet matches, 0 otherwise
 **/
int regexp(regex_t* regex, triplet*  data);

/**
 *  grep the linked list with a regular expression and print matching triplets
 *
 *  \param regex string representation of the regular expression to match against
 *  \param bool_head true to traverse the list from head to tail, false to traverse from tail to head
 *  \param amount maximum number of matches to print. Set to -1 to grep the whole list
 *
 *  \return the number of strings that match the expression
 **/
int grep(char* regex, int bool_head, int amount);

/**
 *  get a string representing the triplet
 *
 *  \param the triplet
 *  \return pointer to a string representation of the triplet
 **/
char* string_triplet(triplet t);

/**
 *  get a string representing the whole list
 *
 *  \return pointer to a string representation of the list
 **/
char* string_list();

#endif /* end of include guard: _TRIPLETS_DATASTRUCT_H_ */