# SOP2020_P01

This README file explains the main elements of our implemention for the SOP project. For explainations about how the data structure is implemented, please refer to the Doc.pdf file. 

## Used Libraries:

+ `<stdio.h>`

+ `<stdlib.h>`

+ `<string.h>`

+ `<regex.h>`

+ `<emscripten.h>`


## Structures:

+ `triplet`  
A triplet is a structure that contains three strings. The size of a string cannot exceed 1024 characters.

+ `linked_list_node`  
A linked\_list\_node is a structure that contains a data payload, a pointer to the next linked\_list\_node and another pointer to the previous linked\_list\_node.


## Variables:

+ `head`  
head is a variable of type pointer to linked\_list\_node, responsible for holding the address of the linked list's head node

+ `tail`  
tail is a variable of type pointer to linked\_list\_node, responsible for holding the address of the linked list's tail node


## Methods:

Signatures of methods (prototypes) of all methods can be found in the triplets\_datastruct.h, test.h and shell.h headers (only methods from triplets\_datastruct are shown in this file).

**Main methods:**

+ `insert`  
**description:** insert method is used to insert a new data triplet into the linked list  
**arguments:** `string s`, `string p`, `string o`  
**returns:** `0` if eveything went correctly, `1` if at least one argument is too big,`3` if the has been a memory allocation error.

+ `match`  
**description:** match method is used to check if the given strings are found as an element of the linked list. If a given string is empty, it also copies the found triplet's data into it.  
**arguments:** `char* s`, `char* p`, `char* o`, `long result`  
**returns:** `0` if a match has been found, `1` otherwise

+ `delete_match`  
**description:** delete method is used to remove the node corresponding to the matched input strings from the linked list. It mainly uses the helper method `delete_node()`  
**arguments:** `char* s`, `char* p`, `char* o`, `long result`  
**returns:** `0` if eveything went correctly, `1` if the deletion could not be performed

**Main Helper methods:**

+ `delete_node`  
**description:** delete\_node method is a helper method used to remove the node corresponding  to the given pointer to linked\_list\_node from the linked list  
**arguments:** `linked_list_node *tmp`  
**returns:** `0` if eveything went correctly, `1` if the deletion could not be performed

+ `isZero`  
**description:** isZero is a helper method used to check if a string is entirely made of zeros or not.  
**arguments:** `char*s`  
**returns:** `1` if the string is made of zeros, `0` otherwise

+ `compare`  
**description:** compare is a helper method used to compare a given set of strings with a triplet's data  
**arguments:** `char*s`, `char*p`, `char*o`, `triplet data`  
**returns:** `1` if they match, `0` otherwise

+ `find`  
**description:** find is a helper method used to find a node corresponding to the given strings and index in the linked list   
**arguments:** `char *s`, `char*p`, `char*o`, `long result`  
**returns:** a pointer to the found `linked_list_node`  


**Input methods:**

+ `getInput`  
**description:** getInput is an input method used to get a string provided as an input from the user (`stdin`) or from another file     
**arguments:** `FILE* f`, `char* input`, `int interactive`  
**returns:** `0` if the string is correctly cought, `1` otherwise (the user is out of attempts)

+ `get_triplet`  
**description:** is an input method used to get three new inputs from `stdin` or another file and insert them into the linked list     
**arguments:** `FILE* f`   
**returns:** `0` if the new triplet could be inserted, `1` otherwise  

+ `read_file`  
**description:** read\_file is an input method used to read data from a file and insert as many new triplets as possible into the linked list. Every 3 successfully read lines will trigger the creation of a new triplet    
**arguments:** `FILE* f`  
**returns:** the amount of new triplets

+ `emptyBuf`  
**description:** emptyBuf is a input-helper method used to empty the filestream buffer.    
**arguments:** `FILE* f`  
**returns:** void  

**Additional features:**

+ `getOccurencesOfChar`  
**description:** getOccurencesOfChar counts the amount of occurences of a given character in the linked list   
**arguments:** `char c`  
**returns:** the amount of occurences  

+ `getOccurencesOfString`  
**description:** getOccurencesOfString counts the amount of occurences of a given string in the linked list  
**arguments:** `char* s`  
**returns:** the amount of occurences   

+ `regexp`  
**description:** regexp checks if a given regular expression matches at least one element of the given triplet. Prints the data if so  
**arguments:** `regex_t* regex`, `triplet*  data`  
**returns:** `1` if at least one element of the triplet matches the regex, `0` otherwise

+ `grep`  
**description:** grep explores the linked list and uses the `regexp()` method to check if the given regular expression is found. Matching triplet are printed. Parameters are used to indicate direction of traversal and amount of matches to print (`-1` prints all matches)      
**arguments:** `char* regex`, `int bool_head`, `int amount`  
**returns:** the amount of triplets that match the regex   

**Printing methods:**

+ `print_index`  
**description:** print_index is a debug method used to print the data of the triplet found in the linked list at the given index   
**arguments:** `int index`  
**returns:** `0` if a triplet of data has been found at the indexed postition, `1` otherwise

+ `print_triplet`  
**description:** print_triplet is a debug method used to print the given triplet  
**arguments:** `triplet t`  
**returns:** void

+ `print_list`  
**description:** print_list is a debug method used to print the entire list. The reverse argument gives the traversal direction.    
**arguments:** `int reverse`   
**returns:** void  

## Build

To build the local target and run the shell, execute the following:  
`make`  
`./main`

To build the WebAssembly target, `TARGET=sop` must be defined when calling make:  
`make TARGET=sop`  

Once the target is built, a webserver needs to be started in order to serve the webpage:  
`make run`  

**Note:** before switching from one target to the other, the build must be cleaned. Either call `make clean` to clean the local target or `make TARGET=sop clean`to clean the WebAssembly target.
