//------------------------------------------------------------------------------
//! \file test.c
//! defines test functions
//
//------------------------------------------------------------------------------
#include "test.h"
#include <string.h>

void functional_test()
{
  printf("\nisZero() tests:\n");
  printf("\tshould be 0: %i\n", isZero("hello"));
  printf("\tshould be 1: %i\n", isZero("00000"));

  printf("\ncompare() tests:\n");
  triplet a = {"a", "b", "c"};

  printf("\tshould be 1: %i\n", compare("0", "0", "0", a));

  printf("\tshould be 1: %i\n", compare("a", "0", "0", a));
  printf("\tshould be 1: %i\n", compare("0", "b", "0", a));
  printf("\tshould be 1: %i\n", compare("0", "0", "c", a));

  printf("\tshould be 1: %i\n", compare("a", "b", "0", a));
  printf("\tshould be 1: %i\n", compare("a", "0", "c", a));
  printf("\tshould be 1: %i\n", compare("0", "b", "c", a));

  printf("\tshould be 1: %i\n", compare("a", "b", "c", a));

  printf("\tshould be 0: %i\n", compare("a", "b", "d", a));

  printf("\ninsert() tests:\n");
  printf("\tlist should be:\tSTART -> END\n");
  printf("\tcurrently is:\t");
  print_list(0);

  insert("a", "b", "c");
  insert("d", "e", "f");
  insert("a", "b", "d");
  insert("a", "b", "c");
  insert("a", "c", "e");

  printf("\tlist should be:\tSTART -> (a, b, c) -> (d, e, f) -> (a, b, d) -> (a, c, e) -> END\n");
  printf("\tcurrently is:\t");
  print_list(0);

  printf("\nmatch() tests:\n");
  char s[SIZE];
  char p[SIZE];
  char o[SIZE];

  strcpy(s, "000000");
  strcpy(p, "000000");
  strcpy(o, "000000");
  printf("\tmatch should return 1: %i\n", match(s, p, o, -1));
  printf("\tdata should be (000000, 000000, 000000): %s, %s, %s\n", s, p, o);

  strcpy(s, "000000");
  strcpy(p, "000000");
  strcpy(o, "000000");
  printf("\tmatch should return 0: %i\n", match(s, p, o, 0));
  printf("\tdata should be (a, b, c): %s, %s, %s\n", s, p, o);

  strcpy(s, "000000");
  strcpy(p, "000000");
  strcpy(o, "000000");
  printf("\tmatch should return 0: %i\n", match(s, p, o, 1));
  printf("\tsdata hould be (d, e, f): %s, %s, %s\n", s, p, o);

  strcpy(s, "000000");
  strcpy(p, "000000");
  strcpy(o, "000000");
  printf("\tmatch should return 0: %i\n", match(s, p, o, 2));
  printf("\tdata should be (a, b, d): %s, %s, %s\n", s, p, o);

  strcpy(s, "000000");
  strcpy(p, "000000");
  strcpy(o, "000000");
  printf("\tmatch should return 0: %i\n", match(s, p, o, 3));
  printf("\tdata should be (a, c, e): %s, %s, %s\n", s, p, o);

  strcpy(s, "000000");
  strcpy(p, "000000");
  strcpy(o, "000000");
  printf("\tmatch should return 1: %i\n", match(s, p, o, 4));
  printf("\tdata should be (000000, 000000, 000000): %s, %s, %s\n", s, p, o);

  strcpy(s, "a");
  strcpy(p, "b");
  strcpy(o, "d");
  printf("\tmatch should return 0: %i\n", match(s, p, o, 0));
  printf("\tdata should be (a, b, d): %s, %s, %s\n", s, p, o);

  strcpy(s, "d");
  strcpy(p, "e");
  strcpy(o, "000000");
  printf("\tmatch should return 1: %i\n", match(s, p, o, 1));
  printf("\tdata should be (d, e, 000000): %s, %s, %s\n", s, p, o);

  printf("\ndelete() tests:\n");

  printf("\tinitial list:\t\t");
  print_list(0);

  strcpy(s, "d");
  strcpy(p, "e");
  strcpy(o, "000000");
  delete_match(s, p, o, 1);
  printf("\tshould stay the same:\t");
  print_list(0);

  strcpy(s, "d");
  strcpy(p, "e");
  strcpy(o, "000000");
  delete_match(s, p, o, 0);
  printf("\tremoved (d, e, f):\t");
  print_list(0);

  strcpy(s, "000000");
  strcpy(p, "000000");
  strcpy(o, "000000");
  delete_match(s, p, o, 1);
  printf("\tremoved second element:\t");
  print_list(0);

  strcpy(s, "a");
  strcpy(p, "000000");
  strcpy(o, "c");
  delete_match(s, p, o, 0);
  printf("\tremoved first element:\t");
  print_list(0);

  strcpy(s, "a");
  strcpy(p, "000000");
  strcpy(o, "e");
  delete_match(s, p, o, 0);
  printf("\tremoved only element:\t");
  print_list(0);
} /* functional_test */

void memory_leak_test()
{
  while(1)
  {
    insert("a", "b", "c");
    delete_match("a", "b", "c", 0);
  }
}

void file_input_test()
{
  FILE * f = fopen("input.txt", "r");

  int items_added = read_file(f);

  printf("Success, added %i items.\n", items_added);
}

void regex_test()
{
  read_file(fopen("input.txt", "r"));
  char digits[] = "^[0-9]+$";
  char alphas[] = "^[a-zA-Z]+$";
  printf("triplets matching /%s/:\n", digits);
  printf("%i triplets matched\n\n", grep(digits, 1, -1));

  printf("10 last triplets matching /%s/:\n", alphas);
  printf("%i triplets matched\n", grep(alphas, 0, 10));
}